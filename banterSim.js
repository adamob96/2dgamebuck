// Javascript file for Banter Simulator 2016
// Written by Adam O'Brien

            var fpsElement = document.getElementById('fps'),
                COLS = 26,
                ROWS = 26,
                EMPTY = 0,
                PLAYER = 1,
                WALL = 2,
                UP = 1,
                DOWN = 3,
                KEY_UP = 38,
                KEY_DOWN = 40,        
                fps = 60,
                lastAnimationTimeFrame,
                lastFpsUpdateTime = 0,
                // These are the game objects
                canvas,
                ctx,
                keystate,
                frames,
                frame_mod,
                level,
                highscore = 0,
                score;
            
// Grid is an object
grid = {
    width:  null,
    height: null,
    _grid:  null,
    
    // This sets up a grid of c columns and r rows, initialised with values d.
    init: function(d, r, c){
        
        this.width = c;
        this.height = r;
        this._grid = [];
        
        for(var x = 0; x < r; x++){
         this._grid.push([]);  
         for(var y = 0; y < c; y++){
             this._grid[x].push(d);  
         }
        }
        
    },
    
    set: function(val, x, y){
        this._grid[x][y] = val;
    },
    
    get: function(x, y){
        return this._grid[x][y];
    }
};

// Player is an object
player = 
{
  direction: null,
  
  init: function(d, x, y){
        this.direction = d;
        this._queue = [];
        this.insert(x,y);
    },
  
  insert: function(x, y){
      //unshift prepends an element to an array
      this._queue.unshift({x:x, y:y});
      this.last = this._queue[0];
  },
  
  remove: function(){
      //pop the last element off the array
      return this._queue.pop();
  }
};

function setWall()
{
   // Sets up the wall in the final column of the canvas
    score++;
    if((score !== 0) && (score % 10 === 0) && (frame_mod > 0))
            {
                level++;
                frame_mod--;
            }
    
    var randpos = Math.floor((Math.random() * grid.height-5)+1);
    
    for(var x = 0; x < grid.width; x++)
    {
        if(x === grid.width - 1)
        {
            for(var y = 0; y < grid.height; y++)
            {
             if((y !== randpos) && (y !== randpos + 1) && (y !== randpos + 2)
                     && (y !== randpos + 3) && (y !== randpos + 4))
             grid.set(WALL, x, y);
            }
        } 
    }
}

function updateWall()
{
    for(var x = 0; x < grid.width; x++)
    {
         for(var y = 0; y < grid.height; y++)
         {
             if(grid.get(x,y) === WALL)
             {
                grid.set(EMPTY, x, y);
                grid.set(WALL, x-1, y);
             }
         }
    } 
}

function checkWallPassed()
{
    for(var x = 0; x < grid.width; x++)
    {
         for(var y = 0; y < grid.height; y++)
         {
             if((grid.get(0,y) === WALL))
             {
                grid.set(EMPTY, x, y);
                
                if(y === grid.height - 1)
                   setWall();
             }
         }
    } 
}

function main()
{
    canvas = document.createElement("canvas");
    canvas.width = 584;
    canvas.height = 415;
    ctx = canvas.getContext("2d");
    // Add cnavas to the body of the document.
    document.body.appendChild(canvas);
    
    frames = 0;
    keystate = {};
    document.addEventListener("keydown", function(evt)
    {
        keystate[evt.keyCode] = true;
    });
    
    document.addEventListener("keyup", function(evt)
    {
        delete keystate[evt.keyCode];
    });
    
    
    init();
    loop();
    
}

function init()
{
    score = -1;
    level = 1;
    frame_mod = 10;
    grid.init(EMPTY, ROWS, COLS);
    var sp = {x:Math.floor(5), y:ROWS/2};
    player.init(UP, sp.x, sp.y);
    grid.set(PLAYER, sp.x, sp.y);
    setWall();
}

function loop()
{
    update();
    // When ready to redraw the canvas, call the loop function
    // Run about 60fps
    window.requestAnimationFrame(loop, canvas);
    
    draw();
}

function update()
{
    frames++;
    //calculateFps(now);
    
    //Change direction of player based on keys
    if((keystate[KEY_UP]) && (player.direction !== UP))
    {
        player.direction = UP;
    }
    if((keystate[KEY_DOWN]) && (player.direction !== DOWN))
    {
        player.direction = DOWN;
    }
    
    //Update the game state
    if(frames % frame_mod === 0)
    {
        var nx = player.last.x;
        var ny = player.last.y;
        //Update player position depending on its direction
        switch(player.direction)
        {
            case UP:
                ny--;
                break;
            case DOWN:
                ny++;
                break;
        }
       
        if(grid.get(nx,ny) === WALL || 0>nx || nx > grid.width - 1 || 0 > ny || ny > grid.height - 1)
        {
            return init();
        }
        
        updateWall();
        
        checkWallPassed();
        
        var play = player.remove();
        grid.set(EMPTY, play.x, play.y);
        
        grid.set(PLAYER, nx, ny);
        player.insert(nx, ny);
 
        
        if(score > highscore)
        {
            highscore = score;
        }
    }
}

function draw()
{
    var tw = canvas.width/grid.width;
    var th = canvas.height/grid.height;
    
    //Iterate through the grid and draw the cells
    for(var x = 0; x < grid.width; x++)
    {
        for(var y = 0; y < grid.height; y++)
        {
            switch(grid.get(x,y))
            {
                case EMPTY:
                    ctx.fillStyle = 'green';
                    break;
                    
                case PLAYER:
                    ctx.fillStyle = 'lightgreen';
                    break;
                    
                case WALL:
                    ctx.fillStyle = 'darkgreen';
                    break;
            }
            
            ctx.fillRect(x*tw, y*th, tw, th);
        }
    }
    
    function calculateFps(now)
    {
        var fps = 1/(now - lastAnimationFrameTime) * 1000;
        if(now - lastFpsUpdateTime > 1000)
        {
                lastFpsUpdateTime = now;
                fpsElement.innerHTML = fps.toFixed(0) + 'fps';
        }
        return fps;
    }

    ctx.fillStyle = "lightgreen";
    ctx.font="20px Times New Roman";
    ctx.fillText("SCORE: " + score, 50, canvas.height - 20);
    ctx.fillText("HIGH SCORE " + highscore, 225, canvas.height - 20);
    ctx.fillText("LEVEL: " + level, 440, canvas.height - 20);
    //ctx.fillText("FPS: " + fps, 50, 30);
}

//Run the game
main();